"use strict";

let divspan = document.createElement("div");
document.body.append(divspan);

let div = document.createElement("div");
document.body.append(div);
div.classList.add("form");

let input = document.createElement("input");
input.placeholder = "Price";
div.append(input);

input.addEventListener("focus", function (e) {
    e.target.style.border = "4px solid green";
});

let currentPrice = document.createElement("span");
divspan.append(currentPrice);

let xBtn = document.createElement("button");
xBtn.innerText = "X";

input.addEventListener("blur", function (e) {
    e.target.style.border = "4px solid ";
    currentPrice.innerHTML = input.value;

    if (input.value < 0 || input.value === "") {
        e.target.style.color = "black";
        e.target.style.border = "4px solid red";
        currentPrice.remove();
        e.target.insertAdjacentHTML("afterEnd", "<br>Please enter correct price");
    } else {
        e.target.style.color = "salmon";
        currentPrice.innerHTML = "Текущая цена: " + input.value;
        divspan.append(currentPrice);
        divspan.append(xBtn);
    }
});

xBtn.addEventListener("click", function (e) {
    input.value = "";
    xBtn.remove();
    currentPrice.remove();
});
